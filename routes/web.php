<?php

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('auth.login');
});

// Auth::routes();
Route::get('login', "Auth\LoginController@showLoginForm")->name("login");
Route::post("/logout", "Auth\LoginController@logout")->name("logout");
Route::post("/login", "Auth\LoginController@login");



Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::middleware(['auth', 'verified'])->group(function () {

    //para creditos
    Route::post("/credito","creditosController@completar_credito");    

    //ir a view cajas
    Route::get("/cajas","generalController@administrar_cajas");
    Route::post("/registrar_caja","generalController@registrar_caja");
    Route::post("/actualizar_caja","generalController@actualizar_caja");
    Route::post("/eliminar_caja","generalController@eliminar_caja");


  //registrar usuarios
    Route::get("/list-user","generalController@listar_usuarios");
    Route::get("/registrar", "generalController@registrar"); //para mostrar la vista de regsitrar
    Route::post("register", "Auth\RegisterController@register")->name("register"); //ejecutar form
    Route::post("/actualizar_user","generalController@actualizar_user");


    //para salidas
    Route::post("/storesalida", "generalController@storesalidas");
    Route::post("/buscarsalidas", "generalController@buscarsalidas");
    Route::post("/eliminarsalida", "generalController@eliminarsalida");
    Route::post("/editar_salida", "generalController@editar_salida");
    //para turnos
    Route::post("/storeturno", "generalController@storeturno");
    Route::post("/cerrarturno", "generalController@cerrarturno");

  
    //para venta
    Route::get("/ventas", "ventasController@index");
    Route::post("/ejecutar_venta", "ventasController@ejecutar_venta");
    Route::post("/cotizacion", "ventasController@cotizacion");
    Route::post("/buscar_venta", "ventasController@buscar_venta"); //buscar ventas por id
    Route::post("/devolucion", "ventasController@devolucion");

    //para articulos##############
    Route::get("/articulos", "articulosController@index");
    Route::post("articulo", "articulosController@store")->name('store_articulo');
    Route::get("buscarArticulo", "articulosController@buscarArticulos")->name('buscarArtiulos');
    Route::post("/buscar_x_codigo", "articulosController@buscar_x_codigo");

    Route::post("buscarExistencia", "articulosController@buscarExistencia")->name('buscarExistencia');
    Route::post("updateExistenca", "articulosController@updateExistenca")->name('updateExistenca');
    Route::post("eliminarArticulo", "articulosController@eliminarArticulo")->name("eliminarArticulo");
    Route::post("buscar_para_editar", "articulosController@buscar_para_editar")->name("buscar_para_editar");
    Route::post("update_articulo", "articulosController@update_articulo");

    Route::post("marcas", "articulosController@store_marca")->name("store_marca");
    Route::post("provedor", "articulosController@store_provedor")->name("store_provedor");
    //################clientes##############
    Route::get("/clientes", "clientesController@index");
    Route::post("cliente", "clientesController@store")->name("storeCliente");
    Route::post("destroycliente", "clientesController@destroy")->name("destroyCliente");
    Route::post("showCliente", "clientesController@showCliente")->name("showCliente");
    Route::post("/updateCliente", "clientesController@update");
    //####################para informes############################
    Route::get("/movimientos", "informesController@movimientos");
    Route::get("/salidas", "informesController@salidas");
    Route::get("/turnos", "informesController@turnos");
    Route::get("/lista_venta", "informesController@ventas");
    //####################para marcas########################
    Route::post("/update_marca", "articulosController@update_marca");
    Route::post("/delete_marca", "articulosController@delete_marca");
    //#####################para provedor#############################
    Route::post("/update_provedor", "articulosController@update_provedor");
    Route::post("/delete_provedor", "articulosController@delete_provedor");
});
