@extends('layouts.app')

@section('content')

    <div class="content">
@if(Session('msj'))
  <div class="alert alert-success alert-dismissible fade show" role="alert">
    No se puede ir a la vista de ventas porque usted no esta asignado a una caja.
      <span aria-hidden="true"  data-dismiss="alert" aria-label="Close" class="float-right text-dark"><strong>X</strong></span>
    <br><br>
  </div>
@endif
        
        <div class="panel-header bg-primary-gradient">
          <div class="page-inner py-5">
            <div class="d-flex align-items-left align-items-md-center flex-column flex-md-row">
              <div>
                <div class="row">
                  <div class="col">
                    <img src="{{asset('img/logo.png')}}" style="width: 120px">
                  </div>
                  <div class="col">
                    <h1 class="text-white pb-2 fw-bold">STEHS Versión 3</h1>
                  <h5 class="text-white op-7 mb-2">By Stehs LATAM</h5>
                  </div>
                </div>


              </div>
              <div class="ml-md-auto py-2 py-md-0">
                <a href="http://stehs.com" class="btn btn-white btn-border btn-round mr-2" target="_blank">Página Oficial</a>
                <p class="text-white">Desarrollo de sistemas web a medida.</p>
                <!-- <a href="#" class="btn btn-secondary btn-round">Add Customer</a> -->
              </div>
            </div>
          </div>
    </div>


        
    <!-- GRAFICAS -->
    <div class="row">

      <div class="col">
        <h2 class="text-center">Ventas x Días</h2>
        <canvas id="graficaventas"></canvas>
      </div>
      <div class="col">

      </div>

    </div>


  </div>



@endsection

@section('script')

<script>
var ctx = document.getElementById('graficaventas').getContext('2d');
var myChart = new Chart(ctx, {
    type: 'line',
    data: {
        labels: @json($dias),//json es propio de blade para obtener arreglo de php
        datasets: [{
            label: 'ventas',
            data: @json($valores),
            backgroundColor: [
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 99, 132, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(54, 162, 235, 1)',
                'rgba(255, 99, 132, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 3
        },
        {
            label: 'creditos',
            data: [1,3,4,5,2,3,0,3,0,0,1,0,0,3,2,5,1,0],
            backgroundColor: [
            'rgba(153, 102, 255, 0.2)'
            ],
            borderColor: [
            'rgba(153, 102, 255, 1)'
            ],
            borderWidth: 1
        }
        ]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});
</script>

@endsection
