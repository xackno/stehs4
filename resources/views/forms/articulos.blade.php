@extends('layouts.app')

@section('content')

<div class="card" style="margin-top: 60px;" id="card1">
	<div class="card-header bg-success" style="width: 100%">
		<div class="row">
			<div class="col-12 col-md-6 col-sm-6 col-lg-6">
				<h2 class="text-white float-left">Articulos <i class="fas fa-box-open"></i></h2>
			</div>
			<div class="col-12 col-md-6 col-sm-6 col-lg-6 col-xl-6">
				<div  style="margin:auto">
					<button style="margin-left: 15px" class=" btn-sm float-right btn btn-secondary" id="btn_a_card2"><i class="fas fa-arrow-right"></i></button>

					
					<button class="btn btn-sm btn-warning float-right" id="btn_open_modal_provedor"> <i class="fas fa-plus"></i>  Provedor</button>
					<button class="btn btn-sm btn-secondary float-right" id="btn_open_modal_marca"> <i class="fas fa-plus"></i> Marca </button>
					@if(count($provedores)!=0 && count($marcas)!=0)

					@if(Auth::User()->type=='administrador')
					<button class="btn btn-sm btn-danger float-right" id="btn_open_existencia"> <i class="fas fa-plus"></i> Existencia</button>
					@endif
					<button class="btn btn-sm btn-primary float-right" id="btn_open_modal_producto"> <i class="fas fa-plus"></i> Articulos</button>
					@endif

				</div>

			</div>
		</div>
	</div>
	<div class="card-body">
		@if(count($provedores)==0)
			<div class="alert alert-danger">Es necesario registrar un <strong>provedor</strong> para poder registrar un articulo</div>
		@endif
		@if(count($marcas)==0)
			<div class="alert alert-danger">Es necesario registrar un una <strong>marca</strong> para poder registrar un articulo</div>
		@endif
		<div id="alertas">
		</div>

		<div class="table-responsive">
			@if(Request::path()!='articulos')
				<a href="{{url('/articulos')}}" class="btn btn-warning btn-sm"><i class="fas fa-eye"></i> Todos</a>
				<br><br>
			@endif
			<table class="table table-bordered table-striped">
				<thead class="bg-primary text-white">
					<tr class="text-center">
						<th>Codigo</th>
						<th>Clave</th>
						<th>Unidad</th>
						<th>Descripción</th>
						<th><i class="fas fa-barcode fa-2x"></i></th>
						@if(Auth::User()->type=='administrador')
						<th>$ compra</th>
						@endif
						<th>$ venta</th>
						<th>Cant</th>
						<th><i class="fas fa-cog"></i></th>
					</tr>
				</thead>
				<tbody>
					@foreach($articulos as $articulo)
						<tr>
							<td class="text-right">{{$articulo->codigo}}</td>
							<td class="text-right">{{$articulo->clave}}</td>
							<td>{{$articulo->unidad}}</td>
							<td>
								<b class="text-secondary">{{$articulo->descripcion}}</b>
							</td>
							<td class="text-center">{{$articulo->cod_barra}}</td>
							@if(Auth::User()->type=='administrador')
								<td class="text-right">${{$articulo->compra}}</td>
							@endif
							<td class="text-right"><b class="text-sucess">${{$articulo->venta}}</b> </td>
							<td class="text-right"><b class="text-danger">{{$articulo->cantidad}}</b> </td>
							<td>
								<button class="btn btn-info btn-sm "  onclick="editar({{$articulo->id}});"  @if(Auth::user()->type!='administrador') disabled='' title='No es administrador' @endif>
									<i class="fas fa-edit "></i></button>

								<button class="btn btn-danger btn-sm " onclick="eliminar({{$articulo->id}});" @if(Auth::user()->type!='administrador') disabled='' title='No es administrador' @endif>
									<i class="fas fa-trash fa-sm"></i>
								</button>

							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
			@if(Request::path()=='articulos')
				{{$articulos->links()}}
			@endif
		</div>
		<style type="text/css">
			.table th{
				/*padding: 50px;*/
				height: 40px;
			}
			.table td{
				height: 35px;
			}
		</style>
	</div>
</div>

<!-- ############################################################################# -->
<!-- ############################################################################# -->

<div class="card" style="margin-top: 60px;" id="card2">
	<div class="card-header bg-success" style="width: 100%">
		<div class="row">
			<div class="col">
				<h2 class="text-white float-left">Tablas <i class="fas fa-table"></i></h2>
			</div>
			<div class="col">
				<button class="float-right btn btn-secondary" id="btn_a_card1"><i class="fas fa-arrow-left"></i></button>
			</div>
		</div>
	</div>
	<div class="card-body">
		<div id="alertas_card2">
		</div>
		<div class="row">
			<div class="col col-lg-6 col-md-6 col-xl-6">
				<h3 class="text-center text-primary">Tabla de marcas</h3>
				<div class="table-responsive">
					<table class="table table-striped table-bordered" id="tabla_marcas">
						<thead class="bg-secondary text-white">
							<tr>
								<th>#</th>
								<th>nombre</th>
								<th>descripción</th>
								<th>Provedor</th>
								<th> <i class="fas fa-cog"></i></th>
							</tr>
						</thead>
						<tbody>
							@foreach($marcas as $marca)
							<tr>
								<td>{{$marca->id_marca}}</td>
								<td>{{$marca->nombre_marca}}</td>
								<td>{{$marca->descripcion_marca}}</td>
								<td>{{$marca->nombre_provedor}}</td>
								<td style="padding: 0px;width: 15%;padding: 0 1px !important;">
									<div class="input-group">
										<i class="d-none">
											{{$nombre=$marca->nombre_marca}}
											{{$descripcion=$marca->descripcion_marca}}
											{{$provedor=$marca->id_provedor}}
										</i>
										<button class="btn btn-sm btn-success " style="margin-right: 40%"
										onclick='editar_marca({{$marca->id_marca}},"{{$nombre}}","{{$descripcion}}","{{$provedor}}");' @if(Auth::user()->type!='administrador') disabled='' title='No es administrador' @endif>

											<i class="fas fa-edit"></i>
										</button>

										<button class="btn btn-sm btn-danger" onclick="eliminar_marca({{$marca->id_marca}});" @if(Auth::user()->type!='administrador') disabled='' title='No es administrador' @endif>
											<i class="fas fa-trash"></i></button>
									</div>
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
			<div class="col col-lg-6 col-md-6 col-xl-6">
				<h3 class="text-center text-primary">Tabla provedores</h3>
				<div class="table-responsive">
					<table class="table table-striped table-bordered" id="tabla_provedores">
						<thead class="bg-warning ">
							<tr>
								<th>#</th>
								<th>nombre</th>
								<th>descripción</th>
								<th>Domicicio</th>
								<th>Teléfono</th>
								<th>E-mail</th>
								<th>RFC</th>
								<th><i class="fas fa-cog"></i></th>
							</tr>
						</thead>
						<tbody>
							@foreach($provedores as $provedor)
								<tr>
									<td>{{$provedor->id}}</td>
									<td>{{$nombre_pro=$provedor->nombre}}</td>
									<td>{{$descri_pro=$provedor->descripcion}}</td>
									<td>{{$domicilio_pro=$provedor->domicilio}}</td>
									<td>{{$tel_pro=$provedor->tel}}</td>
									<td>{{$email_pro=$provedor->email}}</td>
									<td>{{$rfc_pro=$provedor->rfc}}</td>
									<td style="padding: 0px;width:50px;padding: 0 1px !important;">
										<div class="input-group">
										<button class="btn btn-sm btn-success" style="margin-right: 20px" onclick="editar_provedor({{$provedor->id}},'{{$nombre_pro}}','{{$descri_pro}}','{{$domicilio_pro}}','{{$tel_pro}}','{{$email_pro}}','{{$rfc_pro}}' );"  @if(Auth::user()->type!='administrador') disabled='' title='No es administrador' @endif>
											<i class="fas fa-edit"></i>
										</button>

										<button class="btn btn-sm btn-danger" onclick="eliminar_provedor({{$provedor->id}});"  @if(Auth::user()->type!='administrador') disabled='' title='No es administrador' @endif>
											<i class="fas fa-trash"></i>
										</button>
									</div>
									</td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>


	</div>
</div>



<!-- ############################################################################# -->
<!-- ############################################################################# -->






<!--window modal ######modal busqueda par a existencias################-->
  <div class="modal fullscreen-modal fade" id="modal_existencia" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header bg-danger">
        	<span class="text-white" style="font-size: 160%"><i class="fas fa-plus"></i> Existencia</span>
          	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div class="modal-body">
        	<div id="alertasModal"></div>

        	<div class="input-group col-4">
        		<input type="text" id="input_buscar" class="form-control" placeholder="Buscar" autocomplete="off">
        		<button  id="btn_buscar_para_existencia" class="btn  btn-primary"> <i class="fas fa-search"></i> </button>
        	</div>
        	<br>
        	<div class="table-responsive" >
        		<table class="table table-striped table-bordered">
        			<thead class="table-success">
        				<tr>
        					<th>Entradas</th>
        					<th>Existencia</th>
        					<th>Codigo</th>
        					<th >Descripción</th>
        				</tr>
        			</thead>
        			<tbody id="tbody_existencia"></tbody>
        		</table>
        	</div>

        	<button class="btn btn-primary float-right" id="guardar_cambios"><i class="fas fa-save"></i> Guardar cambios</button>

        </div>
      </div>
    </div>
  </div>


<!--window modal ######modal marca################-->
  <div class="modal fullscreen-modal fade" id="modal_agregar_marca" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog modal-sm" role="document">
      <div class="modal-content">
        <div class="modal-header bg-primary">
        	<span class="text-white" style="font-size: 160%"><i class="fas fa-plus"></i> Marca</span>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div class="modal-body">
        	<form id="form_marca">
        		<input type="hidden" name="id_marca" value="">
        		<label>Nombre</label>
	        	<input type="text" name="nombre" class="form-control border-primary" placeholder="Nombre">

	        	<label>Descripción</label>
	        	<input type="text" name="descripcion" class="form-control border-primary" placeholder="Descripción">

	        	<label>Provedor</label>
	        	<select class="form-control border-primary" name="provedor">
	        		<option disabled="" selected="">Secciones una opción</option>
	        		@foreach($provedores as $provedor)
	        			<option value="{{$provedor->id}}">{{$provedor->nombre}}</option>
	        		@endforeach
	        	</select>

	        	<label>Logotipo</label>
	        	<input type="" name="logo" class="form-control" disabled="">
	        	<!-- <input type="file" name="logo" placeholder="logo" class="form-control" > -->
	        	<br><br>
	        	<button type="button" id="btn_store_marca" class="btn btn-success float-right"><i class="fas fa-save"></i> Guardar</button>
	        	<button type="button" id="btn_update_marca" style="display: none" class="btn btn-secondary float-right"> Actualizar</button>
        	</form>

        </div>
      </div>
    </div>
  </div>

<!--window modal ######modal provedor################-->
  <div class="modal fullscreen-modal fade" id="modal_agregar_provedor" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header bg-warning">
        	<span class="text-white" style="font-size: 160%"><i class="fas fa-plus"></i> Provedor</span>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div class="modal-body">
        	<form id="form_provedor">
        		<div class="row">
        			<div class="col">
        				<input type="hidden" name="id_provedor">
        				<label>Nombre</label>
			        	<input type="text" name="nombre" class="form-control" placeholder="Nombre">

			        	<label>Descripción</label>
			        	<input type="text" name="descripcion" class="form-control" placeholder="Descripción">

			        	<label>RFC</label>
			        	<input type="text" name="rfc" class="form-control">
        			</div>
        			<div class="col">
        				<label>Domicilio</label>
			        	<input type="text" name="domicilio" class="form-control">

			        	<label>Telefono</label>
			        	<input type="number" name="tel" class="form-control">

			        	<label>Email</label>
			        	<input type="email" name="email" class="form-control">
        			</div>
        		</div>

	        	<br><br>
	        	<button type="button" id="btn_store_provedor" class="btn btn-success float-right"><i class="fas fa-save"></i> Guardar</button>
	        	<button type="button" id="btn_update_provedor" style="display: none" class="btn btn-secondary float-right"><i class="fas fa-save"></i> Actualizar</button>
        	</form>

        </div>
      </div>
    </div>
  </div>
<!--window modal ######modal agregar productos################-->
  <div class="modal fullscreen-modal fade" id="modalagregarproductos" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header bg-info">
        	<span class="text-white" style="font-size: 160%"><i class="fas fa-plus" id="icon_header"></i> Articulo</span>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div class="modal-body">
        	<form   id="form_articulo">
        		<div class="row">
        		<div class="col">
        			<input type="hidden" name="id_p">
        			<label>Código</label>
        			<input type="text" name="codigo" class="form-control" placeholder="Codigo">

        			<label>Clave</label>
        			<input type="text" name="clave" class="form-control" placeholder="Clave">

        			<label>Unidad</label>
        			<select class="form-control" name="unidad" required="">
        				<option selected="" disabled="">Seleccione uno</option>
        				<option value="pieza">Pieza</option>
        				<option value="kilogramo">Kilogramo</option>
        				<option value="litro">Litro</option>
        				<option value="gramos">Gramos</option>
        				<option value="bolsa">bolsa</option>
        			</select>
        			<label>Descripción</label>
        			<input type="text" name="descripcion" class="form-control border-success" placeholder="descripción" required="">
        		</div>
        		<div class="col">
        			<label>Código de barra</label>
        			<input type="number" name="cod_barra" class="form-control">

        			<label>Precio de compra</label>
        			<input type="number" name="compra" class="form-control" placeholder="precio de compra">

        			<label>Precio de venta</label>
        			<input type="number" name="venta" class="form-control border-success" placeholder="precio de venta" required="">

        			<label>Cantidad</label>
        			<input type="number" name="cantidad" class="form-control" placeholder="cantidad en existencia">
        		</div>
        		<div class="col">
        			<label>Código SAT</label>
        			<input type="text" name="codigo_sat" class="form-control">

        			<label>Descripción SAT</label>
        			<input type="text" name="desc_sat" class="form-control">

        			<label>Marca</label>
        			<select name="marca" class="form-control">
        				<!-- <option selected="" disabled="">Seleccione uno</option> -->
        				@foreach($marcas as $marca)
        					<option value="{{$marca->id_marca}}">{{$marca->nombre_marca}}</option>
        				@endforeach
        			</select>
        		</div>
        	</div>
        	<br><br>
        	<button type="button" class="btn btn-success float-right" id="btn_submit_actualizar" style="display: none">Actualizar</button>
        	<button type="button" id="btn_submit_articulo" class="btn btn-primary float-right"><i class="fas fa-save"></i> Guardar</button>
        	</form>



        </div>
      </div>
    </div>
  </div>



@endsection
@section('script')
<script type="text/javascript">

//######################script para el card 2######################
	$("#btn_update_provedor").click(function(){
		$.ajax({
			url:"{{url('/update_provedor')}}",
			type:"post",
			dataType:"json",
			data:$("#form_provedor").serialize(),
			success:function(e){
				if (e=="success") {
					$("#alertas_card2").html("<div class='alert alert-success'>Modificado corectamente.</div>");
					setInterval(function(){
						location.reload();
					},4000);
					$("#modal_agregar_provedor").modal("hide");
				}else{
					$("#alertas_card2").html("<div class='alert alert-danger'>hubo un error al intentar actualizar este registro, consulte con su administrador.</div>");
					setInterval(function(){
						$("#alertas_card2").html('');
					},4000);
				}
			},error:function(){
				$("#alertas_card2").html("<div class='alert alert-danger'>hubo un error al intentar actualizar este registro, consulte con su administrador.</div>");
					setInterval(function(){
						$("#alertas_card2").html('');
					},4000);
			}
		});
	});

	function editar_provedor(id,nombre,descripcion,domicilio,tel,email,rfc){
		$("#modal_agregar_provedor").modal("show");
		$("#form_provedor")[0].reset();
		$("#btn_update_provedor").show();
		$("#btn_store_provedor").hide();
		$("#form_provedor [name=id_provedor]").val(id);
		$("#form_provedor [name=nombre]").val(nombre);
		$("#form_provedor [name=descripcion]").val(descripcion);
		$("#form_provedor [name=rfc]").val(rfc);
		$("#form_provedor [name=domicilio]").val(domicilio);
		$("#form_provedor [name=tel]").val(tel);
		$("#form_provedor [name=email]").val(email);
	}
	function eliminar_provedor(id){
		msj=confirm("Desea eliminar este registro?, tenga en cuenta que hay MARCAS relacionada a este provedor lo cual no se mostrarán si se elimina este registro.");
		if (msj) {
			$.ajax({
				url:"{{url('/delete_provedor')}}",
				type:"post",
				dataType:"json",
				data:{id:id},
				success:function(e){
					if (e=="success") {
					$("#alertas_card2").html("<div class='alert alert-success'>Eliminado corectamente.</div>");
					setInterval(function(){
						location.reload();
					},4000);
					$("#modal_agregar_provedor").modal("hide");
				}else{
					$("#alertas_card2").html("<div class='alert alert-danger'>hubo un error al intentar actualizar este registro, consulte con su administrador.</div>");
					setInterval(function(){
						$("#alertas_card2").html('');
					},4000);
				}

				},error:function(){
					$("#alertas_card2").html("<div class='alert alert-danger'>hubo un error al intentar actualizar este registro, consulte con su administrador.</div>");
					setInterval(function(){
						$("#alertas_card2").html('');
					},4000);
				}
			});
		}
	}



	$("#btn_update_marca").click(function(){
		$.ajax({
			url:"{{url('/update_marca')}}",
			type:"post",
			dataType:"json",
			data:$("#form_marca").serialize(),
			success:function(e){
				if (e=="success") {
					$("#alertas_card2").html("<div class='alert alert-success'>Modificado corectamente.</div>");
					setInterval(function(){
						location.reload();
					},4000);
					$("#modal_agregar_marca").modal("hide");
				}else{
					$("#alertas_card2").html("<div class='alert alert-danger'>hubo un error al intentar actualizar este registro, consulte con su administrador.</div>");
					setInterval(function(){
						$("#alertas_card2").html('');
					},4000);
				}
			},error:function(e){
				alert("hubo un error al intentar actualizar este registro, consulte con su administrador. ");
			}
		});
	});

	function editar_marca(id,nombre,descripcion,provedor){
		// alert(id+nombre+descripcion+provedor);
		$("#modal_agregar_marca").modal("show");
		$("#form_marca")[0].reset();
		$("#form_marca [name=id_marca]").val(id);
		$("#form_marca [name=nombre]").val(nombre);
		$("#form_marca [name=descripcion]").val(descripcion);
		$("#form_marca [name=provedor]").val(provedor);
		$("#btn_store_marca").hide();
		$("#btn_update_marca").show();

	}
	function eliminar_marca(id){
		msj=confirm("Desea eliminar esta Marca?");
		if (msj) {
			alert("eliminando "+id );
			$.ajax({
				url:"{{url('/delete_marca')}}",
				type:"post",
				dataType:"json",
				data:{id:id},
				success:function(e){
					if (e=="success") {
						$("#alertas_card2").html("<div class='alert alert-success'>Se eliminó corectamente..</div>");
					setInterval(function(){
						location.reload();
					},4000);
					}else{
						$("#alertas_card2").html("<div class='alert alert-danger'>Error al eliminar este registro, consulte con su administrador.</div>");
					setInterval(function(){
						$("#alertas_card2").html('');
					},4000);
					}
				},error:function(){}

			});
		}
	}



	$("#card2").hide();
	$("#btn_a_card1").click(function(){
		$("#card2").hide();
		$("#card1").show();
	});
	$("#btn_a_card2").click(function(){
		$("#card1").hide();
		$("#card2").show();
	});
	$("#tabla_marcas").DataTable({
			"order": [[ 0, 'DESC' ]],
			"language": {
	        "decimal": "",
	        "emptyTable": "No hay información",
	        "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
	        "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
	        "infoFiltered": "(Filtrado de _MAX_ total entradas)",
	        "infoPostFix": "",
	        "thousands": ",",
	        "lengthMenu": "Mostrar _MENU_ Entradas",
	        "loadingRecords": "Cargando...",
	        "processing": "Procesando...",
	        "search": "Buscar:",
	        "zeroRecords": "Sin resultados encontrados",
	        "paginate": {
	            "first": "Primero",
	            "last": "Ultimo",
	            "next": "Siguiente",
	            "previous": "Anterior"
	        }}});
	$("#tabla_provedores").DataTable({
			"order": [[ 0, 'DESC' ]],
			"language": {
	        "decimal": "",
	        "emptyTable": "No hay información",
	        "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
	        "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
	        "infoFiltered": "(Filtrado de _MAX_ total entradas)",
	        "infoPostFix": "",
	        "thousands": ",",
	        "lengthMenu": "Mostrar _MENU_ Entradas",
	        "loadingRecords": "Cargando...",
	        "processing": "Procesando...",
	        "search": "Buscar:",
	        "zeroRecords": "Sin resultados encontrados",
	        "paginate": {
	            "first": "Primero",
	            "last": "Ultimo",
	            "next": "Siguiente",
	            "previous": "Anterior"
	        }}});

//####################################################

	//############btn-actualizar#################
		$("#btn_submit_actualizar").click(function(){

			$.ajax({
				url:"{{url('update_articulo')}}",
				type:"post",
				dataType:"json",
				data:$("#form_articulo").serialize(),
				success:function(e){

					$("#form_articulo")[0].reset();
					$("#alertas").append('<div class="alert alert-success">Articulo actualizado correctamente.</div>');

					setInterval(function(){
						location.reload();
					},3000);
					$("#modalagregarproductos").modal('hide');
				},
				error:function(e){
					$("#alertas").append('<div class="alert alert-danger">Error al actualizar el articulo.</div>');
					setInterval(function(){
						$("#alertas").html('');
					},3000);
					$("#modalagregarproductos").modal('hide');
				}
			});
		});

	//###################editar#################
		function editar(id){
			$("#form_articulo")[0].reset();
			$("#btn_submit_articulo").hide();
			$("#btn_submit_actualizar").show();
			$("#modalagregarproductos").modal("show");
			$("#icon_header").removeClass();
			$("#icon_header").addClass("fas fa-edit");
			$.ajax({
				url:"{{route('buscar_para_editar')}}",
				type:"post",
				dataType:"json",
				data:{id:id},success:function(e){
					$("[name='codigo']").val(e.codigo);
					$("[name='clave']").val(e.clave);
					$("[name='unidad']").val(e.unidad);
					$("[name='descripcion']").val(e.descripcion);
					$("[name='cod_barra']").val(e.cod_barra);
					$("[name='compra']").val(e.compra);
					$("[name='venta']").val(e.venta);
					$("[name='cantidad']").val(e.cantidad);
					$("[name='codigo_sat']").val(e.codigo_sat);
					$("[name='desc_sat']").val(e.desc_sat);
					$("[name='marca']").val(e.marca);
					$("[name='provedor']").val(e.provedor);
					$("[name='id_p']").val(e.id);




				},error:function(e){
					alert("no se encontro!");
				}
			});

		}
	//######################eliminar articulo#################
		function eliminar(id){
			var msj=confirm("Desea eliminar este articulo?");
			if (msj) {
				$.ajax({
					url:"{{route('eliminarArticulo')}}",
					type:"post",
					data:{
						id:id
					},success:function(){
						$("#alertas").append("<div class='alert alert-warning'>Eliminado corectamente</div>");
						setInterval(function(){
							location.reload();
						},2000);
					},error:function(){
						$("#alertas").append("<div class='alert alert-danger'>Error al eliminar este articulo.</div>");
						setInterval(function(){
							$("#alertas").html('');
						},2000);
					}
				});
			}
		}




	//##########################existencia###########
		$("#btn_open_existencia").click(function(){
			$("#modal_existencia").modal("show");
		});
		$("#input_buscar").on("keyup",function(e){
			if (e.keyCode==13) {
				buscarproductos();
				$(this).val('');
			}
		});
		$("#guardar_cambios").hide();

		function buscarproductos(){
			$("#tbody_existencia").html('');
			$.ajax({
				url:"{{route('buscarExistencia')}}",
				type:"post",
				dataType:"json",
				data:{
					buscar:$("#input_buscar").val()
				},success:function(e){
					for(var x=0;x<e.length;x++){
						$("#tbody_existencia").append("<tr> <td style='width:20%'> <input type='number' class='form-control' style='width: 100%'></td><td>"+e[x].cantidad+"</td><td>"+e[x].codigo+" </td><td>"+e[x].descripcion+" </td><td class='d-none'>"+e[x].id+"</td></tr>");
					}
					$("#guardar_cambios").show();
				}
			});
		}
		$("#btn_buscar_para_existencia").click(function(){
			buscarproductos();
			$("#input_buscar").val('');
		});
		//---------------------------------------------
		$("#guardar_cambios").click(function(){
			var id=[]; var cantidad=[];
			$("#tbody_existencia").find("tr td:first-child").each(function(){
				// alert($(this).find('input').val());
				if(parseFloat($(this).find('input').val())){
					cantidad.push($(this).find('input').val());
					id.push($(this).siblings("td").eq(3).html());
				}
          });//fin each
			$.ajax({
				url:"{{route('updateExistenca')}}",
				type:"post",
				dataType:"json",
				data:{
					id:id,
					cantidad:cantidad
				},success:function(e){
					$("#tbody_existencia").html('');
					if (e=="success") {
						$("#alertasModal").append("<div class='alert alert-success'>Modificado correctamente.</div>");
						setInterval(function(){
							$("#alertasModal").html('');
						},3000);
					}
				},error:function(){

						$("#alertasModal").append("<div class='alert alert-danger'>Error al modificar, verifique la información proporcionada...</div>");
						setInterval(function(){
							$("#alertasModal").html('');
						},3000);

				}
			});
		});

	//##################submit provedor###############
		$("#btn_open_modal_provedor").click(function(){
			$("#modal_agregar_provedor").modal('show');
			$("#form_provedor")[0].reset();
			$("#btn_update_provedor").hide();
			$("#btn_store_provedor").show();
		});

		$("#btn_store_provedor").click(function(){
			$.ajax({
				url:"{{route('store_provedor')}}",
				type:"post",
				dataType:"json",
				data:$("#form_provedor").serialize(),
				success:function(e){
					$("#form_provedor")[0].reset();
						$("#alertas").append('<div class="alert alert-success">Provedor agregado correctamente.</div>');
						setInterval(function(){
							$("#alertas").html('');
							location.reload();
						},3000);
						$("#modal_agregar_provedor").modal('hide');
				},error:function(e){
					$("#alertas").append('<div class="alert alert-danger">Error al agregar, verifique la información proporcionada</div>');
						setInterval(function(){
							$("#alertas").html('');
						},3000);
						$("#modal_agregar_provedor").modal('hide');
				}
			});
		});






    //################submit marca#################
		$("#btn_open_modal_marca").click(function(){
			$("#form_marca")[0].reset();
			$("#modal_agregar_marca").modal('show');
			$("#btn_store_marca").show();
			$("#btn_update_marca").hide();

		});

		$("#btn_store_marca").click(function(){
			$.ajax({
				url:"{{route('store_marca')}}",
				type:"post",
				dataType:"json",
				data:$("#form_marca").serialize(),
				success:function(e){
					$("#form_marca")[0].reset();
						$("#alertas").append('<div class="alert alert-success">Marca agregada correctamente.</div>');
						setInterval(function(){
							$("#alertas").html('');
							location.reload();
						},3000);
						$("#modal_agregar_marca").modal('hide');
				},error:function(e){
					$("#alertas").append('<div class="alert alert-danger">Error al agregar, verifique la información proporcionada</div>');
						setInterval(function(){
							$("#alertas").html('');
						},3000);
						$("#modal_agregar_marca").modal('hide');
				}
			});
		});

	//#####################submit articulos#################################
		$("#btn_open_modal_producto").click(function(){
			$("#modalagregarproductos").modal('show');
			$("#btn_submit_actualizar").hide();
			$("#btn_submit_articulo").show();
			$("#form_articulo")[0].reset();
			$("#icon_header").removeClass();
			$("#icon_header").addClass("fas fa-plus");
		});
		$("#btn_submit_articulo").click(function(){
			$.ajax({
				url:"{{route('store_articulo')}}",
				type:"post",
				dataType:"json",
				data:$("#form_articulo").serialize(),
				success:function(e){
					$("#form_articulo")[0].reset();
					$("#alertas").append('<div class="alert alert-success">Articulo agregado correctamente.</div>');
					setInterval(function(){
						$("#alertas").html('');
						location.reload();
					},3000);
					$("#modalagregarproductos").modal('hide');
				},
				error:function(e){
					$("#alertas").append('<div class="alert alert-danger">Error al agregar el articulo.</div>');
					setInterval(function(){
						$("#alertas").html('');
					},3000);
					$("#modalagregarproductos").modal('hide');
				}
			});
		});


</script>
@endsection
