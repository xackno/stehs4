@extends('layouts.app')

@section('content')
<br><br><br>
<div class="card">
	<div class="card-body">
		<h2 class="text-center text-primary"> <i class="fas fa-shopping-basket "></i> VENTAS</h2>
		<div class="table-responsive">
			<table class="table table-striped table-bordered text-center" id="table_ventas">
				<thead class="bg-primary text-white">
					<tr>
						<th>#</th>
						<th>usuario</th>
						<th>turno</th>
						<th>total</th>
						<th>realizado</th>
						<th><i class="fas fa-cog"></i></th>
					</tr>
				</thead>
				<tbody >
					@foreach($ventas as $venta)
					<tr>
						<td>{{ str_pad($venta->id, 8, '0', STR_PAD_LEFT)}}</td>
						<td>{{$venta->usuario}}</td>
						<td>T{{ str_pad($venta->id_turno, 6, '0', STR_PAD_LEFT)}}</td>
						<td class="text-right"><b>${{$venta->total_venta}}</b> </td>
						<td>{{$venta->created_at}}</td>
						<td>
							<button class="btn btn-sm btn-success" onclick="verarticulos({{$venta->id}});"><i class="fas fa-eye"></i></button>
							<button class="btn btn-sm btn-secondary"><i class="fas fa-print"></i></button>

						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>



<!--window modal ######modal busqueda################-->
  <div class="modal fullscreen-modal fade" id="modal_ver_articulos" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header bg-info" style="padding: 2px">
          <h2 id="id_venta_header" class="text-white"></h2>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div class="modal-body">
        	<div class="row">
        		<div class="col text-primary"><span id="turno"></span></div>
        		<div class="col text-success"><span id="usuario"></span></div>
        		<div class="col text-danger"> <span id="fecha"></span></div>
        	</div>
        <div class="table-responsive">
        	<table class="table table-striped table-bordered text-center" id="table_articulos">
        		<thead class="bg-primary text-center text-white">
        			<tr>
        				<th class="d-none">#</th>
        				<th>cantidad</th>
        				<th>unidad</th>
        				<th>descripción</th>
        				<th>precio</th>
        				<th>subtotal</th>
        			</tr>
        		</thead>
        		<tbody>

        		</tbody>
        	</table>
        </div>
        <div>
        	<h2 id="total" class="float-right text-primary"></h2>
        </div>
        </div>
      </div>
    </div>
  </div>



<style type="text/css">
	.table tbody tr:hover{
		background:#3CA567;
		color:white!important;
	}
	.table td, .table th{
		padding-left: 0px !important;
		padding-right: 0px !important;
		height: 30px;
	}
</style>

@endsection
@section('script')
<script type="text/javascript">

function verarticulos(id){
	$("#id_venta_header").html("VENTA: "+  ('00000000' + id).slice(-8));
	$("#table_articulos tbody").html('');
	$.ajax({
		url:"{{url('/buscar_venta')}}",
		type:"post",
		dataType:"json",
		data:{id_venta:id},
		success:function(e){
			$("#turno").html("Turno: "+e["venta"].id_turno);
			$("#usuario").html("Atendió: "+e["venta"].usuario);
			$("#fecha").html("Fecha: "+e["fecha"]);
			$("#total").html("Total: $"+e["venta"].total_venta);
			$("#modal_ver_articulos").modal("show");
			for(var x=0;x<e["articulos"].length;x++){
				var cantidad=e['cantidad'][x];
          		var subtotal=parseFloat(cantidad)* parseFloat(e['articulos'][x].venta);
          		subtotal=subtotal.toFixed(2);

				$("#table_articulos tbody").append("<tr>  <td class='d-none'>"+e["articulos"][x].id+"</td>   <td>"+cantidad+"</td>   <td>"+e['articulos'][x].unidad+"</td>   <td>"+e["articulos"][x].descripcion+"</td>  <td>"+e["articulos"][x].venta+"</td>  <td>"+subtotal+"</td>   </tr>");
			}

		},error:function(){

		}
	});
}









	$("#table_ventas").DataTable({
		"order": [[ 0, 'DESC' ]],
		"language": {
        "decimal": "",
        "emptyTable": "No hay información",
        "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
        "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
        "infoFiltered": "(Filtrado de _MAX_ total entradas)",
        "infoPostFix": "",
        "thousands": ",",
        "lengthMenu": "Mostrar _MENU_ Entradas",
        "loadingRecords": "Cargando...",
        "processing": "Procesando...",
        "search": "Buscar:",
        "zeroRecords": "Sin resultados encontrados",
        "paginate": {
            "first": "Primero",
            "last": "Ultimo",
            "next": "Siguiente",
            "previous": "Anterior"
        }}});

</script>
@endsection
