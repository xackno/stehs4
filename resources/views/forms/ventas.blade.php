@extends('layouts.app')

@section('content')

<div class="row" style="margin: 0px">
  <div class="col-12 col-md-3 col-xl-3 col-lg-3" style="padding: 0">
    <div class="card" style="margin: 0">
      <div class="card-body" style="padding:8px">
        <a  class="btn btn-primary " href="{{url('/home')}}"><i class="fas fa-arrow-left"></i> <i class="fas fa-home"></i> INICIO </a>

        <hr>
        <table class="table table-bordered table-striped" style="width: 70%;margin:auto">
          <tbody>
            <tr>
              <td>FECHA</td><td>{{$fecha}} {{date('Y')}}</td>
            </tr>
            <tr>
              <td>HORA</td><td id="hora_footer"></td>
            </tr>
            <tr>
              <td>CAJERO</td><td>{{Auth::user()->name}}</td>
            </tr>
            <tr>
              <td>TURNO</td><td @if($turno==0) bg-danger text-white @endif>
                @if($turno!=0)
                  <span>Turno:{{$turno}}</span>
                  <input type="hidden" name="turno" id="id_turno" value="{{$turno}}">
                  @else
                  <span class="text-danger">Iniciar TURNO</span>
                @endif
              </td>
            </tr>
              
             <tr> <td>CAJA</td><td>@if(isset($caja->nombre)){{($caja->nombre)}} @endif</td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>


  <div class="col-12 col-md-9 col-lg-9 col-xl-9" style="padding: 0">
    <div class="card" style="height: 90vh;margin-bottom: 0">
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-bordered table-striped">
          <thead class="bg-primary-gradient text-white">
            <tr>
                <th scope="col" class="d-none">#</th>
                <th scope="col">CANTIDAD</th>
                <th scope="col">UNIDAD</th>
                <th scope="col">ARTICULO</th>
                <th scope="col">PRECIO</th>
                <th scope="col">SUBTOTAL</th>
                <th scope="col"><i class="fas fa-cog"></i></th>
              </tr>
          </thead>
          <tbody id="tabla_venta">
            <tr  class="d-none" ><td class="d-non" ></td><td id="td_focus"></td><td></td><td></td></tr>

          </tbody>
        </table>
        </div>
      </div>
    </div>

    <!-- ###############FOOTER######################################### -->
    <div class="card bg-primary"  style="margin-bottom: 0;">
      <div class="card-body" style="padding: 0;margin-top: 10px">
        <div class="row" style="padding: 0;margin: 0">
          <div class="col">
            <div class="input-group">
              <input type="text" name="busqueda"  class="form-control" placeholder="Buscar..." autocomplete="off" id="findProducto" autofocus="true" style="color:black;font-family: 'arial black'">
              <button class="btn btn-sm btn-success" onclick="buscarPro();"><i class="fas fa-search fa-2x" ></i></button>
            </div>
          </div>
          <div class="col">
            <button class="btn btn-sm btn-warning" id="btn_f9"><i class="fas fa-shopping-basket text-dark fa-2x"></i></button>
             <button class="btn btn-sm btn-warning" id="btn_f8"><i class="fas fa-power-off text-dark fa-2x"></i></button>
             <button class="btn btn-sm btn-info" id="btn_f4"><i class="fas fa-sign-out-alt fa-2x text-dark"></i></button>
             <button class="btn btn-sm btn-info" id="btn_devolucion"><i class=" fas fa-hand-point-left fa-2x text-dark"></i></button>
             <div id="alertas"></div>
          </div>
          <div class="col">
            <h1 class="text-white float-right"><span>TOTAL:$</span> <span id="totalpagar">00.00</span> MXN</h1>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<style type="text/css">
        .table  th {
          padding:0px;
          height: 32px;
        }.table  td {
          padding:0px;
          height: 32px;
          font-family: 'arial black';
        }
        #tabla_venta td:nth-child(2){
          color:red;
          font-size: 150%;
        }
        #btn_funciones button{padding: 2px;}
     </style> 




<!-- #################################modales###################################3 -->
<!--window modal ######modal busqueda################-->
  <div class="modal fullscreen-modal fade" id="modal_busqueda" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content" style="height: 500px">
        <div class="modal-header bg-info">
          <h4 id="nivel_cliente"></h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div class="modal-body">
        <div class="table-responsive" style="overflow-x: scroll;overflow-y: scroll;height: 400px">
          <table id="tabla_busqueda" editabled="" class="table table-striped table-bordered" >
               <thead class="table-dark">
                   <tr>
                      <th class="d-none">Id</th>
                      <th>Clave</th>
                      <th >Unidad</th>
                      <th style="width: 50%">Descripción</th>
                      <th>$ Precio</th>
                      <th>Exist</th>
                   </tr>
               </thead>
               <tbody id="tb_busqueda" >
                  <tr  class="d-none" >
                    <td></td>
                    <td></td>
                    <td></td>
                    <td id="start0"></td>
                  </tr>
               </tbody>
           </table>
        </div>
        </div>
      </div>
    </div>
  </div>

<!-- $$$$$$$$$$$$$$$modal cobrar###################### -->
  <div class="modal fade" id="modal_cobrar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header bg-primary" style="padding: 3px">
          <h3 class="text-white">
            <i class="fas fa-shopping-cart"></i>
             Realizar venta
           </h3>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" class="text-white">&times;</span>
        </button>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col text-center">
              <h1 class="text-success">TOTAL:$</h1>
              <h1 id="text-total" class="text-danger">00.00</h1>
            </div>
            <div class="col text-center">
              EFECTIVO:<i class="fas fa-dollar-sign"></i>
              <input type="number" id="efectivo" class="form-control border border-primary">
            </div>
          </div><br>
          <div class="text-center">
            <h3 class="text-primary">Cambio:$<span id="cambio" class="text-danger">00.00</span></h3>
          </div>

        </div>
        <div class="card-footer">
          <!-- <button class="btn btn-success btn-sm float-left" id="btn_a_credito" style="margin-right: 4px"><i class="fas fa-credit-card"></i> Credito</button> -->

          <form  action="{{url('/credito')}}" class="float-left" id="form_credito" target="_blank" method="post" >
            @csrf
            <input type="hidden" name="credito_id_p">
            <input type="hidden" name="credito_cantidad">
            <input type="hidden" name="credito_unidad">
            <input type="hidden" name="credito_descripcion">
            <input type="hidden" name="credito_precio">
            <input type="hidden" name="credito_subtotal">
            <input type="hidden" name="turno" value="{{$turno}}">
            <button class="btn btn-success btn-sm float-left" id="btn_credito"><i class="fas fa-credit-card"></i> Crédito</button>
          </form>

          <form  action="{{url('/cotizacion')}}" class="float-left" id="form_cotizacion" target="_blank" method="post" >
            @csrf
            <input type="hidden" name="id_p">
            <input type="hidden" name="cantidad">
            <input type="hidden" name="unidad">
            <input type="hidden" name="descripcion">
            <input type="hidden" name="precio">
            <input type="hidden" name="subtotal">
            <button class="btn btn-secondary btn-sm float-left" id="btn_cotizacion"><i class="fas fa-table"></i> Cotización</button>
          </form>

          <button class="btn btn-primary btn-sm float-right" onclick="ejecutar_venta();">Realizar <i class=" fas fa-angle-right"></i></button>

        </div>
      </div>
    </div>
  </div>
<!-- fin modal cobrar -->



<!--window modal ######modal turno################-->
  <div class="modal fullscreen-modal fade" id="modal_turno" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog " role="document">
      <div class="modal-content">
        <div class="modal-header bg-info">
          <h3 class="text-white" style="margin:0px" id="header_turno">Turno</h3>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div class="modal-body">
          <!-- #############################from para abrir turno -->
          <form  id="form-store-turno">
            @csrf
            <h4 style="width: 100%">Usuario: {{Auth::user()->name}}<span class="float-right">{{$fecha}}</span> </h4>

            <input type="hidden" name="usuario" value="{{Auth::user()->id}}">
            <input type="hidden" name="status" value="abierto">
            <label><i class="fas fa-dollar-sign"></i> En caja</label>
            <input type="number" name="inicio" class="form-control border-secondary" required="" placeholder="0.00">

            <label><i class="fas fa-comment-dots"></i> Comentario</label>
            <textarea class="form-control border-secondary" name="comentario1" ></textarea>
            <br><br>


            <button id="btn_submit_store_turno" type="button"  class="btn btn-primary btn-sm float-right">Iniciar <i class="fas fa-power-off"></i></button>
          </form>
          <!-- #####################from para cerrar turno -->
          <form action="{{url('/cerrarturno')}}" method="post" id="form-cerrar-turno">
            @csrf
            <h4>Usuario: {{Auth::user()->name}}</h4>
            <input type="hidden" name="id" @if(isset($turno[0])) value="{{$turno[0]->id}}" @endif >
            <input type="hidden" name="usuario" value="{{Auth::user()->id}}">
            <input type="hidden" name="status" value="cerrado">
            <label><i class="fas fa-dollar-sign"></i> En caja</label>
            <input type="number" name="cierre" class="form-control border-secondary" required="" placeholder="0.00">

            <label><i class="fas fa-comment-dots"></i> Comentario</label>
            <textarea class="form-control border-secondary" name="comentario2" ></textarea>



            <br><br>
            <button id="btn_submit_cerrar_turno" type="submit" class="btn btn-danger btn-sm float-right">Cerrar <i class="fas fa-power-off"></i></button>
          </form>
        </div>
      </div>
    </div>
  </div>
<!--window modal ######modal salidas################-->
  <div class="modal fullscreen-modal fade" id="modal_salidas" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog " role="document">
      <div class="modal-content">
        <div class="modal-header bg-info">
          <h3 class="text-white" style="margin:0px">SALIDAS</h3>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div class="modal-body" style="padding-top: 1px">
          <div id="alertasmodal_salidas"></div>
          <div class="input-group">
            <button class="btn btn-primary btn-sm" id="toggle-registrar">Registrar</button>
            <button class="btn btn-primary btn-sm" id="toggle-ver">VER</button>
          </div>
          <div id="registrar_salidas">
              <form id="form_save_salidas">
                <div class="row">
                  <div class="col">
                    <h4 style="width: 100%;padding:0;margin: 0px">Usuario: {{Auth::user()->name}}</h4>
                  </div>

                  <div class="col">
                    @if(isset($turno[0]))
                      <span class="text-danger" style="font-family: 'Arial Black';padding: 0">Turno: {{$turno[0]->id}}</span>
                      @else
                      <span class="text-warning" style="font-family: 'Arial Black'">Iniciar TURNO</span>
                    @endif
                  </div>

                  <div class="col">
                    <h4><span class="float-right">{{$fecha}}</span></h4>
                  </div>
                </div>
                <input type="hidden" name="turno" @if(isset($turno[0])) value="{{$turno[0]->id}}" @endif >
                <input type="hidden" name="usuario" value="{{Auth::user()->id}}">
                <label>Concepto</label>
                <input type="text" name="concepto" class="form-control border-secondary" required="">

                <label><i class="fas fa-dollar-sign"></i> Cantidad</label>
                <input type="number" name="cantidad" id="cantidad_registro_salida" class="form-control border-secondary" placeholder="0.00">
                </form>
          </div>
          <div id="listar_salidas">
            <div class="table_responsive">
              <table class="table table-bordered table-striped" id="tabla_registros_salidas">
                <thead class="table-dark text-center">
                  <tr>
                    <th class="d-none">#</th>
                    <th>concepto</th>
                    <th>cantidad</th>
                    <th><i class="fas fa-cogs"></i></th>
                  </tr>
                </thead>
                <tbody></tbody>
              </table>
              <form class="input-group" id="form_edit_salida">
                <input type="hidden" name="edit_id">
                <input type="text" name="edit_concepto" class="form-control">
                <input type="number" name="edit_cantidad" class="form-control">
                <button type="button" onclick="guardar_edit_salida();" class="btn btn-primary">Guardar <i class="fas fa-save"></i></button>

              </form>
            </div>
          </div>



        </div>
        <div class="modal-footer">
          <button onclick="registrar_salidas();" id="btn-registrar-salida" class="btn btn-primary btn-sm float-right">Registrar</button>
        </div>

      </div>
    </div>
  </div>

<!-- $$$$$$$$$$$$$$$modal devolucion###################### -->
  <div class="modal fade" id="modal_devolucion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header bg-primary" style="padding: 3px">
          <h3 class="text-white">
            <i class="fas fa-shopping-cart"></i>
             Devolución para la  venta <span id="id_venta"></span>
           </h3>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" class="text-white">&times;</span>
        </button>
        </div>
        <div class="modal-body">
          <div id="alert_in_devoluciones"></div>

          <div id="div_info" style="width: 100%">

          </div>
          <div class="table-responsive">
            <table class="table table-bordered table-striped" id="tabla_prod_devolucion">
            <thead class="table-success">
              <tr>
                <th class="d-none">id</th>
                <th>cantidad a devolver</th>
                <th>unidad</th>
                <th>descripcion</th>
                <th>precio</th>
                <th>subtotal</th>
              </tr>
            </thead>
            <tbody></tbody>
          </table>
          </div>
          <h3 class="float-right text-primary">Total de venta: $<span id="total_venta"></span></h3>
          <br><br>
          <h3 class="float-right text-danger">Total a devolver: $<span id="total_devolver"></span></h3>
          <br><br><br><br>
          <button class="btn btn-primary float-right" onclick="guardar_devolucion();">Guardar devolución</button>
          <br>
          <br>
          <div class="table bg-info rounded">
            <p class="text-white"><span class="text-warning">NOTA:</span> Verificar que la mercancia no este dañada antes de aceptar la devolución.</p>
          </div>
        </div>

      </div>
    </div>
  </div>
<!-- fin modal devolucion -->






@endsection
@section('script')
<script type="text/javascript" src="{{asset('/js/for-ventas.js')}}"></script>
<script type="text/javascript" src="{{asset('/js/for-creditos.js')}}"></script>
<script type="text/javascript">
var bloqueador=0;//variable public

function guardar_devolucion(){
  var id_venta=$("#id_venta").html();
  var total_venta=parseFloat($("#total_venta").html())-parseFloat($("#total_devolver").html());
  var id=[];
  var cantidad_devolver=[];
$("#tabla_prod_devolucion tbody").find("tr td:first-child").each(function() {
      id.push($(this).html());
      cantidad_devolver.push(parseFloat($(this).siblings("td").eq(0).find("i").html())-parseFloat($(this).siblings("td").eq(0).find("input").val()));
    });

// alert(cantidad_devolver);
  $.ajax({
    url:"{{url('/devolucion')}}",
    type:"post",
    dataType:"json",
    data:{
      id_venta:id_venta,
      total_venta:total_venta,
      ip_p:id,
      cantidad:cantidad_devolver
    },
    success:function(e){
      buscar_venta(id_venta);
      $("#alert_in_devoluciones").html("<div class='alert alert-success'>Devolución correctamente</div>");
      setInterval(function(){$("#alert_in_devoluciones").html('')},5000);
    },error:function(){
      $("#alert_in_devoluciones").html("<div class='alert alert-danger'>No se pudo guardar la información</div>");
      setInterval(function(){$("#alert_in_devoluciones").html('')},5000);
    }
  });

}

function limpiar_modal_devolucion(){
  $("#tabla_prod_devolucion tbody").html('');
  $("#total_venta,#total_devolver,#id_venta").html('');
}


function buscar_venta(id_venta){
  $("#div_info,#total_venta,#total_devolver").html("");
  $("#tabla_prod_devolucion tbody").html("")
  $.ajax({
    url:"{{url('/buscar_venta')}}",
    type:"post",
    dataType:"json",
    data:{
      id_venta:id_venta
    },success:function(e){
      if (e !=null) {//si existe la venta mostrarla
        $("#div_info").html(
          "<span class='text-primary'>Turno: "+e['venta'].id_turno+"</span>"+
          "  <span>Atendio: "+e['venta'].usuario+"</span>"+
          "<span class='float-right text-danger'>Fecha: "+e['fecha']+"</span>");
          var total_venta=0;
        for(var x=0;x<e['articulos'].length;x++){
          var cantidad=e['cantidad'][x];
          var subtotal=parseFloat(cantidad)* parseFloat(e['articulos'][x].venta);
          subtotal=subtotal.toFixed(2);
          $("#tabla_prod_devolucion tbody").append("<tr><td class='d-none'>"+e['articulos'][x].id+"</td> <td > <input type='number' style='width:60px' value='"+cantidad+"'>     de <i class='d-non float-right'>"+cantidad+"</i></td> <td>"+e['articulos'][x].unidad+"</td> <td>"+e['articulos'][x].descripcion+"</td> <td>"+e['articulos'][x].venta+"</td> <td>"+subtotal+"</td>   </tr>");
          total_venta+=parseFloat(cantidad)* parseFloat(e['articulos'][x].venta);
        }
        $("#total_venta").html(total_venta.toFixed(2));
        $("#total_devolver").html(total_venta.toFixed(2));
      }else{
        $("#alert_in_devoluciones").html("<div class='alert alert-danger'>No se encontró la venta.</div>");
      setInterval(function(){$("#alert_in_devoluciones").html('')},3000);
      }
    },error:function(){
      $("#alert_in_devoluciones").html("<div class='alert alert-danger'>No se encontró la venta.</div>");
      setInterval(function(){$("#alert_in_devoluciones").html('')},3000);
    }
  });
}

//##################calculando cantidad a devolver###########
$("#tabla_prod_devolucion tbody").on("keyup || change","tr td:nth-child(2)",function(){
  var cantidad_original=parseFloat($(this).find("i").html());
  var modificado=parseFloat($(this).find("input").val());
  // alert(cantidad_original+" " + modificado);
  if (modificado>cantidad_original || modificado<1) {//si es mayor a la cantidad original mandar alerta
    $(this).find("input").val(cantidad_original);
    $("#alert_in_devoluciones").html("<div class='alert alert-danger'>No puede ser mayor ni menor a 1</div>");
      setInterval(function(){$("#alert_in_devoluciones").html('')},2000);
      //calculando e lsubtotal
      var precio=parseFloat($(this).siblings("td").eq(3).html());
      var subtotal=cantidad_original*precio;
      $(this).siblings("td").eq(4).html(subtotal.toFixed(2));
  }else{
    var precio=parseFloat($(this).siblings("td").eq(3).html());
    var subtotal=modificado*precio;
   $(this).siblings("td").eq(4).html(subtotal.toFixed(2));
  }
  //calculando total de $ a devolver
  total=0;
  $("#tabla_prod_devolucion tbody").find("tr td:last-child").each(function() {
      total+=parseFloat($(this).html());
    });
  $("#total_devolver").html(total.toFixed(2));
});



function storeturno(){
  if (bloqueador==0) {
    bloqueador=1;
    $.ajax({
      url:"{{url('/storeturno')}}",
      type:"post",
      data:$("#form-store-turno").serialize(),
      beforeSend:function(){
        bloqueador=1;
      },
      success:function(e){
        $("#form-store-turno")[0].reset();
          $("#modal_turno").modal("hide");
          location.reload();
          setInterval(bloqueador=0,1000);
        bloqueador=0;
      },error:function(e){

      }
    });
  }
}


$("#form_edit_salida").hide();
function editarsalida(id,concepto,cantidad){
  $("#form_edit_salida").show();
  $("[name=edit_id]").val(id);
  $("[name=edit_concepto]").val(concepto);
  $("[name=edit_cantidad]").val(cantidad);
}
function guardar_edit_salida(){
  $.ajax({
    url:"{{url('/editar_salida')}}",
    type:"post",
    dataType:"json",
    data:{
      id:$("[name=edit_id]").val(),
      concepto:$("[name=edit_concepto]").val(),
      cantidad:$("[name=edit_cantidad]").val(),
    },success:function(e){
      buscarsalidas();
      $("#form_edit_salida")[0].reset();
      $("#form_edit_salida").hide();
      $("#alertasmodal_salidas").html("<div class='alert alert-success text-white '>Modificado corectamente.</div>");
      setInterval(function(){
        $("#alertasmodal_salidas").html('');
      },4000);
    },error:function(){
      $("#alertasmodal_salidas").html("<div class='alert alert-danger text-white '>Error al modificar.</div>");
      setInterval(function(){
        $("#alertasmodal_salidas").html('');
      },4000);
    }
  });
}



function buscarsalidas(){
  $("#tabla_registros_salidas tbody").html('');
  $.ajax({
    url:"{{url('/buscarsalidas')}}",
    type:"post",
    dataType:"json",
    data:{
      user:{{Auth::user()->id}},
      turno:$("#id_turno").val()
    },
    success:function(e){
      for(var x=0;x<e.length;x++){
        var concepto=e[x].concepto;
        $("#tabla_registros_salidas tbody").append("<tr><td class='d-none'>"+e[x].id+"</td><td>"+e[x].concepto+"</td> <td class='text-right'>"+e[x].cantidad+"</td><td><button class='btn btn-danger btn-sm' onclick='eliminarsalida("+e[x].id+");'> <i class='fas fa-trash'></i></button> <button class='btn btn-sm btn-success' onclick='editarsalida("+e[x].id+",\""+e[x].concepto+"\",\""+e[x].cantidad+"\");'><i class='fas fa-edit'></i></button></td> </tr>");
      }
    },error:function(){
      alert("Error al buscar registros");
    }
  });
}

function eliminarsalida(id){
  // alert(id);
  var msj=confirm("Desea eliminar este registro?");
  if (msj) {
    $.ajax({
    url:"{{url('/eliminarsalida')}}",
    type:"post",
    dataType:"json",
    data:{id:id},
    success:function(e){
      buscarsalidas();
      $("#alertasmodal_salidas").html("<div class='alert alert-success text-white '>Se eliminó corectamente.</div>");
      setInterval(function(){
        $("#alertasmodal_salidas").html('');
      },4000);
    },error:function(){
      $("#alertasmodal_salidas").html("<div class='alert alert-danger text-white '>No se pudo eliminar</div>");
      setInterval(function(){
        $("#alertasmodal_salidas").html('');
      },4000);
    }
  });
  }

}



//###############registrar salidas##########
function registrar_salidas(){

  if ($("[name=concepto]").val()!="" && $("#cantidad_registro_salida").val()!="" && bloqueador==0) {
    bloqueador=1;
    $.ajax({
    url:"{{url('/storesalida')}}",
    type:"post",
    dataType:"json",
    data:$("#form_save_salidas").serialize(),
    success:function(e){
      $("#form_save_salidas")[0].reset();
      $("#alertasmodal_salidas").html("<div class='alert alert-success text-white '>"+e+"</div>");
      setInterval(function(){
        $("#alertasmodal_salidas").html('');
      },4000);
      bloqueador=0;
    },error:function(){
      $("#alertasmodal_salidas").html("<div class='alert alert-danger text-white '>ERROR AL GUARDAR ESTE REGISTRO</div>");
      setInterval(function(){
        $("#alertasmodal_salidas").html('');
      },4000);
    }
  });
  }else{
    alert("Necesita llenar todos los campos.");
  }

}



//###########################ejecutar venta##########################

function ejecutar_venta(){
  var id_p=[];
  var cantidad=[];
  var unidad=[];
  var descripcion=[];
  var precio=[];
  var subtotal=[];
  var cont=0;

  $("#tabla_venta").find("tr td:first-child").each(function(){
    if (cont>0) {
      id_p.push($(this).html());
      cantidad.push($(this).siblings("td").eq(0).html());
      unidad.push($(this).siblings("td").eq(1).html());
      descripcion.push($(this).siblings("td").eq(2).html());
      precio.push($(this).siblings("td").eq(3).html());
      subtotal.push($(this).siblings("td").eq(4).html());
    }
    cont++;
  });
  //-------------------------------------------------------------

  if (id_p.length>0 && bloqueador==0) {//si existe productos en la tabla de venta
      bloqueador=1;
    $.ajax({
      url:"{{url('ejecutar_venta')}}",
      type:"post",
      dataType:"json",
      data:{
        id_p:id_p,
        cantidad:cantidad,
        unidad:unidad,
        descripcion:descripcion,
        precio:precio,
        subtotal:subtotal,
        efectivo:$("#efectivo").val(),
        user:"{{Auth::user()->name}}",
        id_turno:$("#id_turno").val()
      },success:function(e){
        // alert(e);
        $("#alertas").html("<div class='bg-success text-white rounded'>Venta correta</div>");
        setInterval(function(){
          $("#alertas").html('');
        },3000);
        $("#modal_cobrar").modal("hide");
        $("#efectivo").val('');
        limpiartablaventa();
        bloqueador=0;
        $("#findProducto").focus();
      },error:function(){
        alert("No se pudo realizar la venta.");
      }
    });
  }else{//si no hay producto en la tabla de venta
    bloqueador=0;
    alert("No hay producto en la tabla de venta.");
  }

}


//####################funcion buscar por codigo de barras#############
    function buscar_x_codigo(codigo){

      $.ajax({
        url:"{{url('/buscar_x_codigo')}}",
        type:"post",
        dataType:"json",
        data:{codigo:codigo},
        success:function(e){
          if (ya_existe(e[0].id)==0) {
              $("#tabla_venta").append("<tr tabindex='0' class='move'>  <td class='d-none'>"+e[0].id+"</td> <td contenteditable=''>1</td> <td>"+e[0].unidad+"</td> <td class='text-primary'>"+e[0].descripcion+"</td> <td>"+e[0].venta+"</td> <td>"+e[0].venta+"</td> <td onclick='eliminarPRO();'><i class='fas fa-trash text-danger'></i></td> </tr>");
            $("#findProducto").val('');
            $("#findProducto").focus();
            calcularTotal();
            }
            $("#findProducto").val('');
        },error:function(){
          alert("No se encontro el producto");
        }
      });
    }

//################FUNCION DE BUSCAR PRODUCTO############################
  function buscarPro(){
    limpiartablabusqueda();
    $('#modal_busqueda').modal('show');
    var producto=$("#findProducto").val();
        $.ajax({
          url:'{{route("buscarExistencia")}}',
          type:'post',
          dataType:'json',
          data:{
            buscar:producto
          },success:function(e){
             if (e=="") {
               $("#tabla_busqueda tbody").append("<tr  class='text-danger'> <td  colspan='6'><b>No se encontró registro!!</b></td></tr>");
              }else{
                for(var x=0;x<e.length;x++){
                $("#tabla_busqueda tbody").append("<tr tabindex='0' class='move'><td width='1px' class='d-none'>"+e[x].id+
                  "</td>   <td>"+e[x].clave+
                  "</td>   <td>"+e[x].unidad+
                  "</td>   <td>"+e[x].descripcion+
                  "</td>   <td>"+e[x].venta+
                  "</td>   <td>"+e[x].cantidad+
                  "</td>  </tr>");
               }//fin for
              }
          },error:function(){
            alert("Hubo un problema al buscar este producto");
          }
            });
            $("#tabla_busqueda tbody tr[tabindex=0]").focus();
  }



</script>
@endsection
