<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Turnos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('turnos', function (Blueprint $table) {
            $table->id("id");
            $table->integer("usuario");
            $table->decimal('inicio', 12, 2)->nullable();
            $table->decimal('cierre', 12, 2)->nullable();
            $table->text('comentario1')->nullable();
            $table->text('comentario2')->nullable();
            $table->datetime('fecha_fin')->nullable();
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('turnos');
    }
}
