<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class articulos extends Model
{
    use HasFactory;
    
     protected $fillable = [
        'id',
        'codigo',
        'clave',
        'unidad',
        'descripcion',
        'cod_barra',
        'compra',
        'venta',
        'codigo_sat',
        'desc_sat',
        'marca',
        'provedor',
        'cantidad',
        'caducidad'
    ];
}
