<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class clientes extends Model
{
    use HasFactory;
    protected $fillable = [
        'id_cliente',
        'nombre_cliente',
        'identificacion',
        'poblacion',
        'colonia',
        'calle',
        'municipio',
        'rfc',
        'tel',
        'email'
    ];
}
