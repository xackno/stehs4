<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cajas extends Model
{
    use HasFactory;
    protected $fillable = [
        'id',
        'nombre',
        'mac',
        'marca_impresora',
        'nombre_impresora',
        'mm_impresora',
        'created_at',
        'updated_at',
    ];

}
