<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class turnos extends Model
{
    use HasFactory;
    protected $fillable = [
        'turnos',
        'id',
        'usuario',
        'inicio',
        'cierre',
        'comentario1',
        'comentario2',
        'fecha_fin',
        'status',
    ];
}
