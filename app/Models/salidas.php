<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class salidas extends Model
{
    use HasFactory;
    protected $fillable = [
        'id',
        'usuario',
        'turno',
        'cantidad',
        'concepto',
        'created_at',
        'updated_at',
    ];
}
