<?php

namespace App\Http\Controllers;

use App\Models\salidas;
use App\Models\turnos;
use Illuminate\Http\Request;
use App\Models\cajas;
use App\Models\User;
class generalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }
    public function listar_usuarios(){
        $users=User::all();
        $cajas=cajas::all();
        return view("auth.list-user",compact('users','cajas'));
    }
    public function actualizar_user(Request $data){
        $user=User::find($data->get('id'));
        try {
            $user->update($data->all());
        } catch (Exception $e) {
            var_dump($e);
        }
        return back();
    }


    public function administrar_cajas(){
        $cajas=cajas::all();
        return view("forms.cajas",compact('cajas'));
    }
    public function actualizar_caja(Request $data){
        $caja=cajas::find($data->get('id'));
        try {
            $caja->update($data->all());
            $status="success";
        } catch (Exception $e) {
            $status="error";
        }
        return back();
    }
    public function eliminar_caja(Request $data){
        
        try {
            $caja=cajas::find($data->get('id'));
            $caja->delete();
            $status="success";

            $usuarios=User::where("caja","=",$data->id);
            $usuarios->update(["caja"=>NULL]);
        } catch (Exception $e) {
            $status="fails";
        }


        return json_encode($status);
    }
    public function registrar_caja(Request $data){

        try {
            $cajas=cajas::create($data->all());
            $status="success";
        } catch (Exception $e) {
            $status="fail";
        }
        // var_dump($data->nombre);
        return json_encode($status);
    }

    public function registrar()
    {
        return view('auth.register');
    }

    public function storeturno(Request $data)
    {
        try {
            $turno  = turnos::create($data->all());
            $status = "success";
        } catch (Exception $e) {
            $status = "fail";
        }

        return json_encode($status);
    }
    public function buscarsalidas(Request $data)
    {
        $user    = $data->get("user");
        $turno   = $data->get("turno");
        $salidas = salidas::where("turno", "=", $turno)
            ->where("usuario", "=", $user)->get();
        return json_encode($salidas);
    }
    public function editar_salida(Request $data)
    {
        $id       = $data->get("id");
        $concepto = $data->get("concepto");
        $cantidad = $data->get("cantidad");
        try {
            $salida = salidas::find($id);
            $salida->update($data->all());
            $status = "success";
        } catch (Exception $e) {
            $status = "fail";
        }
        return json_encode($status);
    }
    public function eliminarsalida(Request $data)
    {
        $id = $data->get("id");
        try {
            $salida = salidas::find($id);
            $salida->delete();
            $status = "success";
        } catch (Exception $e) {
            $status = "fail";
        }
        return json_encode($status);
    }
    public function storesalidas(Request $data)
    {
        try {
            $salida = salidas::create($data->all());
            $status = "SE REGISTRO CORRECTAMENTE";
        } catch (Exception $e) {
            $status = "fail";
        }

        return json_encode($status);
    }

    public function cerrarturno(Request $data)
    {
        date_default_timezone_set('America/Mexico_City');
        $id     = $data->get("id");
        $cerrar = turnos::find($id);
        $cerrar->update(
            [
                "cierre"      => $data->get("cierre"),
                "comentario2" => $data->get("comentario2"),
                "fecha_fin"   => now(),
                "status"      => $data->get("status"),
            ]
        );
        return back();
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
