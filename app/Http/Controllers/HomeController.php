<?php

namespace App\Http\Controllers;

use App\Models\ventas;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $primer_registro = ventas::all()->first();
        if (isset($primer_registro->created_at)) {
            $primera_fecha   = $primer_registro->created_at;
            $primera_fecha   = explode(" ", $primera_fecha); //fecha primer registro
            $fecha_actual    = date("Y-m-d"); //fecha actual
            //
            $meses   = ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"];
            $sumando = $primera_fecha[0];
            $cont    = 1;
            $dias    = array();
            $valores = array();
            while (strtotime($sumando) <= strtotime($fecha_actual)) {
                $fecha_formato = explode("-", $sumando);
                $fecha_formato = $fecha_formato[2] . "-" . $meses[$fecha_formato[1] - 1]; //formateando fecha como (dia - mes)
                array_push($dias, $fecha_formato);
                $ventas = ventas::select("COUNT(*)")
                    ->where("created_at", "LIKE", "$sumando%");
                array_push($valores, $ventas->count());
                $sumando = date("Y-m-d", strtotime($primera_fecha[0] . "+ " . $cont . " days"));
                $cont++;
            }
        }else{
            $dias=["0"];
            $valores=["0"];
        }
        

        return view('home', compact("dias", "valores"));
    }
}
