//#############DEVOLUCIONES###########################
$("#btn_devolucion").click(function() {
    var id_venta = prompt("Introduce el número de venta para poder hacer la devolución");
    if (!isNaN(id_venta) && id_venta != null && id_venta != "") {
        $("#modal_devolucion").modal("show");
        $("#id_venta").html(id_venta);
        buscar_venta(id_venta);
    } else {
        alert("dato invalido");
    }
});
//############registrar salidas en turno##############
$("#btn_f4").click(function() {
    if (validar_turno() != 0) {
        $("#modal_salidas").modal("show");
    } else {
        alert("INICIAR TURNO");
    }
});
//##############################
$("#toggle-registrar,#listar_salidas").hide();
$("#toggle-registrar").click(function() {
    $("#listar_salidas").hide();
    $(this).hide();
    $("#toggle-ver,#registrar_salidas,#btn-registrar-salida").show();
});
$("#toggle-ver").click(function() {
    buscarsalidas();
    $("#registrar_salidas,#btn-registrar-salida").hide();
    $(this).hide();
    $("#toggle-registrar,#listar_salidas").show();
});
// ###################turno##############
validar_turno();
$("#btn_f8").click(function() {
    $("#modal_turno").modal("show");
});
$("#btn_submit_store_turno").click(function() {
    //     $("#form-store-turno").submit();
    storeturno();
});
$("#btn_submit_cerrar_turno").click(function() {
    $("#form-cerrar-turno").submit();
    // alert("fsdf");
});

function validar_turno() {
    var turno = $("#id_turno").val();
    if (turno != null) { //abierto
        $("#header_turno").html("CERRAR TURNO");
        $("#form-store-turno,#btn_submit_store_turno").hide();
        $("#form-cerrar-turno,#btn_submit_cerrar_turno").show();
        return turno;
    } else { //cerrado
        $("#header_turno").html("ABRIR TURNO");
        $("#form-store-turno,#btn_submit_store_turno").show();
        $("#form-cerrar-turno,#btn_submit_cerrar_turno").hide();
        return 0;
    }
}
//#############################cotizacion######################
$("#btn_cotizacion").click(function(e) {
    e.preventDefault();
    var id_p = [];
    var cantidad = [];
    var unidad = [];
    var descripcion = '';
    var precio = [];
    var subtotal = [];
    var cont = 0;
    $("#tabla_venta").find("tr td:first-child").each(function() {
        if (cont > 0) {
            id_p.push($(this).html());
            cantidad.push($(this).siblings("td").eq(0).html());
            unidad.push($(this).siblings("td").eq(1).html());
            descripcion += ($(this).siblings("td").eq(2).html()) + "=>";
            precio.push($(this).siblings("td").eq(3).html());
            subtotal.push($(this).siblings("td").eq(4).html());
        }
        cont++;
    });
    $("[name=id_p]").val(id_p);
    $("[name=cantidad]").val(cantidad);
    $("[name=unidad]").val(unidad);
    $("[name=descripcion]").val(descripcion);
    $("[name=precio").val(precio);
    $("[name=subtotal]").val(subtotal);
    $("#form_cotizacion").submit();
});
//
//
//
//
//
//
//
//####################ejecutar###venta######################
//####################ejecutar###venta######################
$("#efectivo").keyup(function(e) { //ejecutar la venta con un enter
    if (e.keyCode == 13) {
        ejecutar_venta();
    }
});











//-------------------teclas de funciones-------------
$("body").keyup(function(e) {
    if (e.keyCode == 120) { //F9
        if (validar_turno() != 0) {
            var cont=0;
            $("#tabla_venta").find("tr td:first-child").each(function(){
                cont++;//comprobando si hay productos en la tabla
              });
            if (cont>1) {//abrir modal solo si hay productos en la tabla de venta
                $("#modal_cobrar").modal("show");
                $("#text-total").html($("#totalpagar").html());
            }else{
                alert("¡¡¡NO HAY PRODUCTOS EN LA TABLA");
            }
        } else {
            alert("INICIAR TURNO");
        }
    }
    if (e.keyCode == 119) { //F8
        $("#modal_turno").modal("show");
    }
    if (e.keyCode == 115) { //F4
        if (validar_turno() != 0) {
            $("#modal_salidas").modal("show");
        } else {
            alert("INICIAR TURNO");
        }
    }
});
//------------------------------en el modal realizar ventas-------------------------------------------
$("#btn_f9").click(function() {
    if (validar_turno() != 0) {
        var cont=0;
        $("#tabla_venta").find("tr td:first-child").each(function(){
            cont++;
          });
            if (cont>1) {//abrir modal solo si hay productos en la tabla de venta
                $("#modal_cobrar").modal("show");
                $("#text-total").html($("#totalpagar").html());
            }else{
                alert("¡¡¡NO HAY PRODUCTOS EN LA TABLA!!!");
            }
    } else {
        alert("INICIAR TURNO");
    }
});
$('#modal_cobrar').on('shown.bs.modal', function() { //al lanzar el modal de realizar venta enfocar al importe
    $("#efectivo").focus();
});
$("#efectivo").on("keyup", function() {
    var efectivo = $(this).val();
    var total = $("#text-total").html();
    var cambio = parseFloat(efectivo) - parseFloat(total);
    $("#cambio").html(cambio.toFixed(2));
});
//----------------------------------------------------------------------------
$("#tabla_venta").on("keyup", "tr td:nth-child(2)", function(e) { //al modificar la cantidad
    var cantidad = $(this).html();
    var precio = $(this).siblings("td").eq(3).html();
    var subtotal = parseFloat(cantidad) * parseFloat(precio);
    $(this).siblings("td").eq(4).html(subtotal.toFixed(2));
    calcularTotal();
});
//####################bloquear el enter en un td#############################
$('#tabla_venta').bind("keypress", "tr td", function(e) {
    if (e.which == 13) {
        $("#findProducto").focus();
        return false;
    }
});
$("#findProducto").on("keyup", "", function(e) { //realizar busqueda
    if (e.keyCode == 39) { //flecha derecha , realizar busqueda 
        buscarPro();
    }
    if (e.keyCode == 13) { //bucar por codigo de barras
        var codigo = $("#findProducto").val();
        buscar_x_codigo(codigo);
    }
    $("body").keyup(function(e) { ///al presionar la tecla ESC enfocar al buscardor
        if (e.keyCode == 27) {
            $("#findProducto").focus();
        }
    });
});
$("#findProducto").on("keyup", function(e) { //enfocar en tabla venta
    if (e.keyCode == 38) { //flecha arriba
        var id_elem = "td_focus";
        iniciarFocus2(id_elem);
    }
});
$('#modal_busqueda').on('shown.bs.modal', function() { //al lanzar el modal iniciar focus en la tabla de busqueda
    var id_elem = "start0";
    iniciarFocus(id_elem);
});
//##########################agregar a la tabla ventas con doble click#########################
$("#tb_busqueda").on("dblclick", "tr", function() {
    $("#modal_busqueda").modal("hide");
    var id = $(this).find("td").eq(0).html();
    var unidad = $(this).find("td").eq(2).html();
    var desc = $(this).find("td").eq(3).html();
    var precio = $(this).find("td").eq(4).html();
    if (ya_existe(id) == 0) {
        $("#tabla_venta").append("<tr tabindex='0' class='move'>  <td class='d-none'>" + id + "</td> <td contenteditable=''>1</td> <td>" + unidad + "</td> <td class='text-primary'>" + desc + "</td> <td>" + precio + "</td> <td>" + precio + "</td> <td onclick='eliminarPRO();'><i class='fas fa-trash text-danger'></i></td> </tr>");
        $("#findProducto").val('');
        $("#findProducto").focus();
        calcularTotal();
    }
});

function agregando_a_tabla_venta(start) {
    $("#modal_busqueda").modal("hide");
    var id = $(start).siblings("td").eq(0).html();
    var unidad = $(start).siblings("td").eq(2).html();
    var desc = $(start).html();
    var precio = $(start).siblings("td").eq(3).html();
    iniciarFocus("start0");
    if (ya_existe(id) == 0) {
        $("#tabla_venta").append("<tr tabindex='0' class='move'>  <td class='d-none'>" + id + "</td> <td contenteditable=''>1</td> <td>" + unidad + "</td> <td class='text-primary'>" + desc + "</td> <td>" + precio + "</td> <td>" + precio + "</td> <td onclick='eliminarPRO();'><i class='fas fa-trash text-danger'></i></td> </tr>");
        $("#findProducto").val('');
        $("#findProducto").focus();
        calcularTotal();
    }
}

function ya_existe(id) {
    var yaExiste = 0;
    $("#tabla_venta").find("tr td:first-child").each(function() {
        var id_art = $(this).html();
        var precio = $(this).siblings("td").eq(3).html();
        if (id == id_art) {
            yaExiste = 1;
            $(this).siblings("td").eq(0).html(parseFloat($(this).siblings("td").eq(0).html()) + 1);
            var cantidad = $(this).siblings("td").eq(0).html();
            var subtotal = parseFloat(cantidad) * parseFloat(precio);
            $(this).siblings("td").eq(4).html(subtotal.toFixed(2));
            calcularTotal();
            $("#findProducto").focus();
        }
    }); //fin each
    return yaExiste;
}
//#########################funcion limpiar tabla##################
function limpiartablabusqueda() {
    var contEach = 0;
    start = document.getElementById('start0');
    start.focus();
    start.style.backgroundColor = 'green';
    start.style.color = 'white';
    document.onkeydown = checkKey;
    $("#tb_busqueda").find("tr td:first-child").each(function() {
        if (contEach > 0) {
            var a = $(this).parent();
            $(a).remove();
        } //fin if
        contEach++;
    }); //fin each
}
//#########################funcion limpiar tabla##################
function limpiartablaventa() {
    var contEach = 0;
    start = document.getElementById('td_focus');
    start.focus();
    start.style.backgroundColor = 'green';
    start.style.color = 'white';
    document.onkeydown = checkKey;
    $("#cambio,#totalpagar").html("00.00");
    $("#tabla_venta").find("tr td:first-child").each(function() {
        if (contEach > 0) {
            var a = $(this).parent();
            $(a).remove();
        } //fin if
        contEach++;
    }); //fin each
}
//##############ELIMINAR PRODUCTOS DE LA TABLA#####################
var eliminarPRO = function() {
    $("#tabla_venta").on("click", "tr td:last-child", function() {
        var a = $(this).parent();
        $(a).remove();
        calcularTotal();
    });
}

function calcularTotal() {
    total = 0;
    $("#tabla_venta").find("tr td:nth-child(6)").each(function() {
        total += parseFloat($(this).html());
    });
    total = total.toFixed(2);
    $("#totalpagar").html(total);
}
// $("body").click(function(){
//   getFullscreen(document.documentElement);
// });











function getFullscreen(element) {
    if (element.requestFullscreen) {
        element.requestFullscreen();
    } else if (element.mozRequestFullScreen) {
        element.mozRequestFullScreen();
    } else if (element.webkitRequestFullscreen) {
        element.webkitRequestFullscreen();
    } else if (element.msRequestFullscreen) {
        element.msRequestFullscreen();
    }
}
//##########################focus para tb-busqueda##########################################  
var start;

function iniciarFocus(id_elem) {
    start = document.getElementById(id_elem);
    start.focus();
    document.onkeydown = checkKey;
}
//#############FUNCION PARA RECORER LA TABLA DE BUSQUEDA CON LAS FLECHAS#######################
function dotheneedful(sibling) {
    if (sibling != null) {
        start.focus();
        start.style.backgroundColor = '';
        start.style.color = '';
        sibling.focus();
        sibling.style.backgroundColor = 'green';
        sibling.style.color = '#fff';
        start = sibling;
    }
}

function checkKey(e) {
    e = e || window.event;
    if (e.keyCode == '13') {
        if ($(start).html() != '') {
            agregando_a_tabla_venta(start);
        }
    }
    if (e.keyCode == '38') { //flecha arriba
        var idx = start.cellIndex;
        var nextrow = start.parentElement.previousElementSibling;
        if (nextrow != null) {
            var sibling = nextrow.cells[idx];
            dotheneedful(sibling);
        }
    } else if (e.keyCode == '40') { //flecha abajo
        var idx = start.cellIndex;
        var nextrow = start.parentElement.nextElementSibling;
        if (nextrow != null) {
            var sibling = nextrow.cells[idx];
            dotheneedful(sibling);
        }
    }
} //fin checkkey


//#######################recorer tabla venta con flechas##########################################
var start2;

function iniciarFocus2(id_elem) {
    start2 = document.getElementById(id_elem);
    start2.focus();
    document.onkeydown = checkKey2;
}

function dotheneedful2(sibling) {
    if (sibling != null) {
        start2.focus();
        start2.style.backgroundColor = '';
        start2.style.color = '';
        sibling.focus();
        sibling.style.backgroundColor = 'green';
        sibling.style.color = '#fff';
        start2 = sibling;
    }
}

function checkKey2(e) {
    e = e || window.event;
    if (e.keyCode == '38') { //flecha arriba
        var idx2 = start2.cellIndex;
        var nextrow2 = start2.parentElement.previousElementSibling;
        if (nextrow2 != null) {
            var sibling2 = nextrow2.cells[idx2];
            dotheneedful2(sibling2);
        } else {
            dotheneedful2(start2);
            $("#findProducto").focus();
        }
    } else if (e.keyCode == '40') { //flecha abajo
        var idx2 = start2.cellIndex;
        var nextrow2 = start2.parentElement.nextElementSibling;
        if (nextrow2 != null) {
            var sibling2 = nextrow2.cells[idx2];
            dotheneedful2(sibling2);
        } else {
            dotheneedful2(start2);
            $("#findProducto").focus();
        }
    } else if (e.keyCode == '46') { //tecla suprimir un producto
        // a=$(this).parent();
        $(start2).parent().remove();
        calcularTotal();
        $("#findProducto").focus();
    }
} //fin checkkey